import { RouterModule,Routes }  from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { SingupComponent } from './components/singup/singup.component';
import {dashboard_routes} from './components/dashboard/dashboard.route';
import {AutoGuardService} from './service/auth-gard.service';

const APP_ROUTES:Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SingupComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: dashboard_routes,
    canActivate: [ AutoGuardService ]
  },
  { path: '**', pathMatch:'full', redirectTo:'' }
];

export const app_routing = RouterModule.forRoot(APP_ROUTES);
