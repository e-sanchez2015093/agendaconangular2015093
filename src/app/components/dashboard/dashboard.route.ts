import {Routes} from '@angular/router';
import {BodyComponent} from './body/body.component';
import {HomeComponent} from './home/home.component';
import {CategoriaComponent} from './categoria/categoria.component';
import {ContactoComponent} from './contacto/contacto.component';
import {TareaComponent} from './tarea/tarea.component';
import {UsuarioComponent} from './usuario/usuario.component';
import { ContactoDetalleComponent } from './contacto/contacto-detalle.component';
import { TareaDetalleComponent } from './tarea/tarea-detalle.component';
import {usuario_routes} from './usuario/usuario.route';
import {contact_routes} from './contacto/contacto.route';
import {CitaComponent} from './cita/cita.component';
import { HistorialComponent } from './historial/historial.component';
// dashboard/categoria
export const dashboard_routes:Routes = [
    {path:'categoria',component:CategoriaComponent},
    {path:'contacto',component:ContactoComponent},
    {path:'tarea',component:TareaComponent},
    { path: 'tarea/:idTarea', component: TareaDetalleComponent },
    {path:'home',component:HomeComponent},
    {path:'body',component:BodyComponent},
    {path:'usuario',component:UsuarioComponent},
    {path:'historial',component:HistorialComponent},
    {path:'contacto/:idContacto',component:ContactoDetalleComponent},
    //{path:'tarea/:idTarea',component:TareaDetalleComponent},
   // {path:'tarea/:idTarea',component:TareaDetalleComponent},
    {path:'usuario',
    component:UsuarioComponent,
    children:usuario_routes
},
    {path:'**',pathMatch:'full',redirectTo:'contacto'}
]