import { Component, OnInit } from '@angular/core';
import {HistorialService} from '../../../service/historial.service';
@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {
  historial:any[] =[];
  constructor(private historialservice:HistorialService) { }

  ngOnInit() {
    this.inicializar();
  }
  private inicializar(){
    this.historialservice.getHistoriales().subscribe(data =>{
      this.historial = data;
      console.log(this.historial);
    });
  }

}
