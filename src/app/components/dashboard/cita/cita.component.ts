import { Component, OnInit } from '@angular/core';
import {CitaService} from '../../../service/cita.service';
@Component({
  selector: 'app-cita',
  templateUrl: './cita.component.html'
})
export class CitaComponent implements OnInit {
  citasComponent:any[] =[];
  constructor(
    private citaService:CitaService
  ) { }

  ngOnInit() {
    this.inicializar();
  } 
  private inicializar(){
    this.citaService.getCitas().subscribe(data =>{
      this.citasComponent = data;
      console.log(this.citasComponent);
    });
  }
  public borrarCita(idCita:number){
    this.citaService.eliminarCita(idCita)
      .subscribe(res =>{
        this.inicializar();
      });
  }
}
