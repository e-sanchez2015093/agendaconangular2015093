import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { TareaService } from '../../../service/tarea.service';
@Component({
  selector: 'app-tarea',
  templateUrl: './tarea-detalle.component.html'
})
export class TareaDetalleComponent implements OnInit {
  url:string;
  notificacion:any = {
    estado:false,
    mensaje: ""
  };
    tarea:any = {
        titulo:"",
        descripcion:"",
        categoria:"",
        prioridad:0,
        fecha_final:""
    };
    constructor(
        private router:Router,
        private activatedRoute: ActivatedRoute,
        private tareaService:TareaService){
          this.activatedRoute.params.subscribe(params =>{
            this.url = params["idTarea"];
            if(this.url !=="nuevo"){
              this.tareaService.getTarea(this.url)
              .subscribe(c => this.tarea = c);
            }else{
              this.resetFormulario();
            }
          })
        }
    ngOnInit() {}
    guardarCambios(){
      if(this.url === "nuevo"){
        this.tareaService.nuevaTarea(this.tarea)
        .subscribe(res =>{
          console.log(res);
          this.setNotificacion(res);
          this.resetFormulario();
        });
      }else{
        console.log(this.tarea);
        this.tareaService.editarTarea(this.tarea,this.url)
        .subscribe(res =>{
          console.log(res);
          this.setNotificacion(res);
        });
      }
    }

    private setNotificacion(notificacion:any) {
    this.notificacion.estado = notificacion.estado;
    this.notificacion.mensaje = notificacion.mensaje;

    setTimeout(() => {
      this.resetNotificacion();
    }, 5000);
  }
  private resetFormulario(){
    this.tarea.titulo = "";
    this.tarea.descripcion = "";
    this.tarea.categoria = "";
    this.tarea.prioridad = 0;
    this.tarea.fecha_final = 0;
  }
    private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }

}