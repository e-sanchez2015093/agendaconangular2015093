import { Component, OnInit } from '@angular/core';
import {TareaService} from '../../../service/tarea.service';
@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html'
})
export class TareaComponent implements OnInit {
    tareas:any[] = [];
  constructor(
    private tareaService:TareaService
  ){ }

  ngOnInit() {
   this.inicializar();
  }
  private inicializar(){
    this.tareaService.getTareas().subscribe(data =>{
      this.tareas = data;
      console.log(data)
    });
  }
  public borrarTarea(idTarea:number){
    this.tareaService.eliminarTarea(idTarea)
      .subscribe(res =>{
        if(res.estado){
          this.inicializar();
        }
      })
  }

}
