import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../../service/contacto.service';
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html'
})
export class ContactoEditarComponent implements OnInit {

  constructor(private _contactoService:ContactoService) { }

  ngOnInit() {
    this._contactoService.getContacto().subscribe();
  }

}
