import { Routes } from '@angular/router';

import { ContactoDetalleComponent } from './contacto-detalle.component';
import { ContactoEditarComponent } from './contacto-editar.component';
import { ContactoComponent } from './contacto.component';

export const contact_routes:Routes = [
  { path: '', component: ContactoComponent },
  { path: 'detalle', component: ContactoDetalleComponent },
  { path: 'editar', component: ContactoEditarComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }
]