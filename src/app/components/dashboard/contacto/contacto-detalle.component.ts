import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { ContactoService } from '../../../service/contacto.service';
import { CategoriaService } from '../../../service/categoria.service';
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto-detalle.component.html'
})
export class ContactoDetalleComponent implements OnInit {
  url:string;
  categorias:any[] = [];
  notificacion:any = {
    estado:false,
    mensaje: ""
  }
  contacto:any = {
    nombre: "",
    apellido: "",
    direccion: "",
    telefono: "",
    correo:"",
    foto:"",
    idCategoria: 0,
    idContacto: 0
  }

  constructor(
    
    private router:Router,
    private activatedRoute: ActivatedRoute,
    private contactoService: ContactoService,
    private categoriaService:CategoriaService) {
    this.activatedRoute.params.subscribe(params => {
      this.url = params["idContacto"];

       if(this.url !== "nuevo") {
        this.contactoService.getContactoProfe(this.url)
        .subscribe(c => this.contacto = c);
      } else {
        this.resetFormulario();
      }
      });
     }

  ngOnInit() {
    this.categoriaService.getCategoria().subscribe();
  }
    guardarCambios() {
    if(this.url === "nuevo") {
      this.contactoService.nuevoContacto(this.contacto)
      .subscribe(res => {
        console.log(res);
        this.setNotificacion(res);
        this.resetFormulario();
      });
    } else {
      console.log(this.contacto);
      this.contactoService.editarContacto(this.contacto, this.url)
      .subscribe(res => {
        console.log(res);
        this.setNotificacion(res);
      });
    }
  }
    private setNotificacion(notificacion:any) {
    this.notificacion.estado = notificacion.estado;
    this.notificacion.mensaje = notificacion.mensaje;

    setTimeout(() => {
      this.resetNotificacion();
    }, 5000);
  }
    private resetFormulario() {
    this.contacto.nombre = "";
    this.contacto.apellido = "";
    this.contacto.direccion = "";
    this.contacto.telefono = "";
    this.contacto.foto = "";
    this.contacto.idCategoria = 0;
    this.contacto.idContacto = 0;
  }
    private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }
  public fileUpload(event){
    console.log('Eventod esta adentro')
    console.log(event.target.files[0]);
    this.contactoService.upload(event.target.files[0])
      .subscribe(ruta =>{
        console.log(JSON.stringify(ruta));
        this.contacto.foto = ruta.path
      },error =>{
        console.log(error);
      }, () =>{
        console.log('Peticion Terminada');
      })
  }



}
