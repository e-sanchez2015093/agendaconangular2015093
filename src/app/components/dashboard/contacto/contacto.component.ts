import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../../service/contacto.service';
@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html'
})
export class ContactoComponent implements OnInit {
contactos:any[] = [];
  constructor(
    private _contactoService:ContactoService
  ) { }

  ngOnInit() {
    //this._contactoService.getContacto().subscribe();
    this.inicializar();
  }

 private inicializar() {
    this._contactoService.getContactosProfe().subscribe(data => {
      this.contactos = data;
      
    })
  }
  public borrarContacto(idContacto:number) {
    console.log(idContacto);
    this._contactoService.eliminarContacto(idContacto)
    .subscribe(res => {
      if(res.estado) {
        this.inicializar();
      }
    })
  }

}
