import { Component, OnInit } from '@angular/core';
import {CategoriaService} from '../../../service/categoria.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var $:any;

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html'
})
export class CategoriaComponent implements OnInit {
  formularioAgregar:FormGroup;
  categoriaCargada:any;
  formularioEditar:FormGroup;
  hidden: boolean = false;

  constructor(private _categoriaService:CategoriaService) {}
  hideModal:boolean = false;
  ngOnInit() {
    
    let validaciones = [
      Validators.required, Validators.minLength(2)
    ];
    this._categoriaService.getCategoria().subscribe();
    this.formularioAgregar = new FormGroup({
      'nombreCategoria':new FormControl('',validaciones),
    });
    this.formularioEditar = new FormGroup({
      idCategoria:new FormControl(''),
      'nombreCategoria':new FormControl('')
    });
  }
  public agregar(){
    console.log(this.formularioAgregar.value);
    this._categoriaService.agregar(this.formularioAgregar.value);
    this._categoriaService.getCategoria().subscribe();
    $('#myModalCategoriaNueva').modal('hide');
    this.ngOnInit();
  }
  public eliminar(idCategoria:number){
    console.log(idCategoria);
    this._categoriaService.eliminar(idCategoria);
    this._categoriaService.getCategoria().subscribe();
    this.ngOnInit();

  }
  public cargar(item:any){
    this.categoriaCargada = item;
    console.log(this.categoriaCargada);
  }
  public editar(){
    this.formularioEditar.controls.idCategoria.setValue(this.categoriaCargada.idCategoria);
    if(this.formularioEditar.value.nombreCategoria == ''){
      this.formularioEditar.controls.nombreCategoria.setValue(this.categoriaCargada.nombreCategoria);
    }
    console.log(this.formularioEditar.value);
    this._categoriaService.editar(this.formularioEditar.value);
    $('#myModalEditarCategoria').modal('hide');
    this.limpiarActualizar();
  }
  public limpiarActualizar(){
    this._categoriaService.getCategoria().subscribe();
    this.formularioEditar.controls.idCategoria.setValue('');
    this.formularioEditar.controls.nombreCategoria.setValue('');
  }


}
