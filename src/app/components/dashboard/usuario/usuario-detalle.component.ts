import { Component, OnInit } from '@angular/core';
import{UsuarioService} from '../../../service/usuario.service';
@Component({
  selector: 'app-usuario-datelle',
  templateUrl: './usuario.component.html'
})
export class UsuarioDetalleComponent implements OnInit {
  
  constructor(private _usuarioService:UsuarioService) { }

  ngOnInit() {
    this._usuarioService.getUsuarios().subscribe();
  }

}