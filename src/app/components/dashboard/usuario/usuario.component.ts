import { Component, OnInit } from '@angular/core';
import{UsuarioService} from '../../../service/usuario.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit {
  formularioAgregar:FormGroup;
  formularioEditar:FormGroup;
  usuarioCargado:any;
  constructor(private _usuarioService:UsuarioService) { 
    

  }

  ngOnInit() {

        let validaciones = [
      Validators.required, Validators.minLength(4)
    ]

    this._usuarioService.getUsuarios().subscribe();

       this.formularioAgregar = new FormGroup({
      'nick': new FormControl('',validaciones),
      'contrasena': new FormControl('',validaciones)
    });

      this.formularioEditar = new FormGroup({
      idUsuario: new FormControl(''), 
      'nick': new FormControl(''),
      'contrasena': new FormControl('')
    });
  }
  public agregar(){
    console.log(this.formularioAgregar.value);
    this._usuarioService.agregar(this.formularioAgregar.value);
    this._usuarioService.getUsuarios().subscribe();
  }
  public cargar(item:any){
    this.usuarioCargado = item;
    console.log('Esta en el cargar');
    console.log(this.usuarioCargado);
  }
  public editar(){
    this.formularioEditar.controls.idUsuario.setValue(this.usuarioCargado.idUsuario);
    if(this.formularioEditar.value.nick == '' && this.formularioEditar.value.contrasena == ''){
        console.log("Entro al primer if");
        this.formularioEditar.controls.nick.setValue(this.usuarioCargado.nick);
        this.formularioEditar.controls.contrasena.setValue(this.usuarioCargado.contrasena);  
    } else if(this.formularioEditar.value.nick == ''){
      console.log("Entro al primer else");
      this.formularioEditar.controls.nick.setValue(this.usuarioCargado.nick);
    } else if(this.formularioEditar.value.contrasena == ''){
      console.log("Entro al segundo else");
        this.formularioEditar.controls.contrasena.setValue(this.usuarioCargado.contrasena);  
    }

   
      console.log(this.formularioEditar.value);
      this._usuarioService.editar(this.formularioEditar.value);
      this.limpiarActualizar();
 
   
  }
  public eliminar(idUsuario:number){
    console.log(idUsuario);
    this._usuarioService.eliminar(idUsuario);
    this._usuarioService.getUsuarios().subscribe();
  } 
  public limpiarActualizar(){
     this._usuarioService.getUsuarios().subscribe();
     this.formularioEditar.controls.idUsuario.setValue('');
     this.formularioEditar.controls.nick.setValue('');
     this.formularioEditar.controls.contrasena.setValue('');
  }

}
