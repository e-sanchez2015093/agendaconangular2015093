import { Component, OnInit } from '@angular/core';
import{UsuarioService} from '../../../service/usuario.service';
@Component({
  selector: 'app-usuario-editar',
  templateUrl: './usuario.component.html'
})
export class UsuarioEditarComponent implements OnInit {
  
  constructor(private _usuarioService:UsuarioService) { }

  ngOnInit() {
    this._usuarioService.getUsuarios().subscribe();
  }

}