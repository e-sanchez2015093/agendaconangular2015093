import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {UsuarioService} from './usuario.service';

import 'rxjs/add/operator/map';
@Injectable()
export class CitaService{
    url:string = "http://localhost:3000";
    citas:any[];
    constructor(
        private http:Http,
        private usuarioService:UsuarioService
    ){}
    getCitas(){
        let uri = `${this.url}/api/v1/cita/`;
        let headers = new Headers({
            'Authorization': this.usuarioService.getToken()
        });
        return this.http.get(uri,{headers})
            .map(res =>{
                return res.json();
            })
    }
    getCita(idCita:any){
        let uri = `${this.url}/api/v1/cita/${idCita}`;
        let headers = new Headers({
        'Authorization': this.usuarioService.getToken()
        });
        return this.http.get(uri,{headers})
            .map(res =>{
                console.log(res.json());
                return res.json();
            });
    }
    nuevaCita(cita:any){
        let uri = `${this.url}/api/v1/cita`;
        let headers = new Headers({
         'Content-Type': 'application/json',
         'Authorization': this.usuarioService.getToken()  
        });
        let data = JSON.stringify(cita);
        return this.http.post(uri,data,{headers})
        .map(res =>{
            return res.json();
        })
    }
    editarCita(cita:any,idCita:any){
        let uri = `${this.url}/api/v1/cita/${idCita}`;
        let headers = new Headers({
            'Content-Type':'application/json',
            'Authorization':this.usuarioService.getToken()
        })
        let data = JSON.stringify(cita);
        return this.http.put(uri,data,{headers})
        .map(res =>{
            return res.json();
        })
    }
    eliminarCita(idCita:number){
        let uri = `${this.url}/api/v1/cita/${idCita}`;
        let headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.usuarioService.getToken()
        });
        return this.http.delete(uri,{headers})
        .map(res =>{
            return res.json();
        })
    }

}