import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {UsuarioService} from './usuario.service';

import 'rxjs/add/operator/map';
@Injectable()
export class ContactoService {
    uriContacto = "http://localhost:3000/api/v1/contacto/";
     url:string = "http://localhost:3000";
    contactos:any[];
    constructor(private _http:Http,private usuarioService:UsuarioService) {}
    getContacto(){
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({'headers': headers});
        headers.append('Authorization', token);
        console.log(JSON.stringify(options));
        return this._http.get(this.uriContacto, options)
        .map(res => {
            console.log(res.json());
            this.contactos = res.json();
        });
    }

  getContactosProfe() {
    let uri = `${this.url}/api/v1/contacto/`;

    let headers = new Headers({
      'Authorization': this.usuarioService.getToken()
    });

    return this._http.get(uri, {headers})
    .map(res => {
      return res.json();
    })
  }

  getContactoProfe(idContacto:any) {
    let uri = `${this.url}/api/v1/contacto/${idContacto}`;

    let headers = new Headers({
      'Authorization': this.usuarioService.getToken()
    });

    return this._http.get(uri, {headers})
    .map(res => {
      console.log(res.json());
      return res.json();
    })
  }

   nuevoContacto(contacto:any) {
    let uri = `${this.url}/api/v1/contacto`;

    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
    });

    let data = JSON.stringify(contacto);

    return this._http.post(uri, data, {headers})
    .map(res => {
      return res.json();
    });
  }

  editarContacto(contacto:any, idContacto:any) {
    let uri = `${this.url}/api/v1/contacto/${idContacto}`;

    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
    });

    let data = JSON.stringify(contacto);

    return this._http.put(uri, data, {headers})
    .map(res => {
      return res.json();
    });
  }
  eliminarContacto(idContacto:number) {
    console.log(idContacto);
    let uri = `${this.url}/api/v1/contacto/${idContacto}`;
    console.log(uri);

    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
    });

    return this._http.delete(uri, {headers})
    .map(res => {
      return res.json();
    });
  }

  public upload(file:any){
    console.log("Esta en el service");
    console.log("El file: " + file);
    console.log("file name");
    console.log(file.name);
    let uri = "http://localhost:3000/upload";
    let token = localStorage.getItem('token');
    let headers = new Headers({'Accept':'application/json'});
    headers.append('Authorization',token);
    let options = new RequestOptions({'headers':headers});

    let formData = new FormData();
    formData.append('file',file,file.name);
    return this._http.post(uri,formData,options)
      .map(res => {
        return res.json();
      })

  }



}