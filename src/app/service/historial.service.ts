import {Injectable} from '@angular/core';
import {Http,Headers,RequestOptions} from '@angular/http';
import {UsuarioService} from './usuario.service';
import 'rxjs/add/operator/map';
@Injectable()
export class HistorialService{
    url:string = "http://localhost:3000";
    historials:any[];
    constructor(
        private http:Http,
        private usuarioService:UsuarioService,

    ){}
    getHistoriales(){
        let uri = `${this.url}/api/v1/historial/`;
        let headers = new Headers({
          'Authorization': this.usuarioService.getToken()  
        });
        return this.http.get(uri,{headers})
        .map(res =>{
            return res.json();
        })
    }
}