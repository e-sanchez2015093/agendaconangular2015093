import {Injectable} from '@angular/core';
import {Http, Headers,RequestOptions} from '@angular/http';
import {UsuarioService} from './usuario.service';
import 'rxjs/add/operator/map';
@Injectable()
export class TareaService{
    url:string = "http://localhost:3000";
    headers:Headers;
    constructor(private http:Http, private usuarioService:UsuarioService){
           
        
    }
    public getTareas(){
       let uri = `${this.url}/api/v1/tarea/`;
       let headers = new Headers({
        'Authorization': this.usuarioService.getToken()
       });
       return this.http.get(uri,{headers})
        .map(res=>{
            return res.json();
        });
    }
    getTarea(idTarea:any){
        let uri = `${this.url}/api/v1/tarea/${idTarea}`;
        let headers = new Headers({
            'Authorization': this.usuarioService.getToken()
        });
        return this.http.get(uri,{headers})
            .map(res => {
               console.log(res.json());
               return res.json();
            })
    }
     nuevaTarea(tarea:any){
        let uri = `${this.url}/api/v1/tarea`;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': this.usuarioService.getToken()
        });
        let data = JSON.stringify(tarea);
        return this.http.post(uri,data,{headers})
            .map(res=>{
                return res.json();
            });
    }
     editarTarea(tarea:any,idTarea:any){
        let uri = `${this.url}/api/v1/tarea/${idTarea}`;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': this.usuarioService.getToken()
        });
        let data = JSON.stringify(tarea);
        return this.http.put(uri,data,{headers})
            .map(res =>{
                return res.json();
            });


    }
     eliminarTarea(idTarea:number){
        let uri = `${this.url}/api/v1/tarea/${idTarea}`;
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': this.usuarioService.getToken()
        });
        return this.http.delete(uri,{headers})
            .map(res =>{
                return res.json();
            })
        
    }

}