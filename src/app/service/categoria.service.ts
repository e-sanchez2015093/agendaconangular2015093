import { Injectable } from '@angular/core';
import { Headers, Http, Request, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class CategoriaService{
    uriCategoria = "http://localhost:3000/api/v1/categoria";
    categorias:any[];
    constructor(private _http:Http){}
    public getCategoria(){
        let token = localStorage.getItem('token');
        let usuario = localStorage.getItem('currentUser');
        console.log(JSON.stringify(usuario));
        console.log(usuario);

        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({'headers':headers});
        headers.append('Authorization',token);
        return this._http.get(this.uriCategoria,options)
            .map(res =>{
                console.log(res.json());
                this.categorias = res.json();
            });
    }
    public agregar(categoria:any){
        let uri = "http://localhost:3000/api/v1/categoria";
        let token = localStorage.getItem('token');
        
        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({'headers':headers});
        headers.append('Authorization',token);
        let data = JSON.stringify(categoria);
        this._http.post(uri,data,options)
            .subscribe(res =>{
                
            },error =>{
                console.log(error.text());

            })


    }
    public editar(categoria:any){
        let uri = "http://localhost:3000/api/v1/categoria/";
        let uri2 = uri + categoria.idCategoria;
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({'headers':headers});
        headers.append('Authorization', token);
        let data = JSON.stringify(categoria);
        this._http.put(uri2,data,options)
            .subscribe(res =>{

            },error =>{
                console.log(error.text());
            });


    }
    public eliminar(idCategoria:any){
        let uri = "http://localhost:3000/api/v1/categoria/";
        let uri2 = uri + idCategoria;
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type':'application/json'});
        let options = new RequestOptions({'headers':headers});
        headers.append('Authorization', token);
        this._http.delete(uri2,options)
            .subscribe(res =>{

            },error =>{
                console.log(error.text());
            });
    }

}

