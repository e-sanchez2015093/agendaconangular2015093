import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UsuarioService {
  uriUsuario = "http://localhost:3000/";
  usuarios:any[];

  constructor(
    private http:Http,
    private router:Router
  ) {  }

  public autenticar(usuario:any) {
    this.uriUsuario += 'auth/';
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({
      headers: headers
    });

    let data = JSON.stringify(usuario);

    this.http.post(this.uriUsuario, data, options)
    .subscribe( res => {
      console.log(res.json());

      this.setToken(res.json().token);

      this.setCurrentUser({
        nick: res.json().nick,
        idUsuario: res.json().idUsuario
      });

      this.router.navigate(['/dashboard']);
    }, error => {
      console.log(error.text());
    });
  }

  public setToken(token:string) {
    if(localStorage.getItem('token') != token) {
      localStorage.removeItem('token');
      localStorage.setItem('token', token);
    }
  }

  public getToken():string {
    return localStorage.getItem('token');
  }

  public setCurrentUser(usuario:any) {
   // localStorage.setItem('currentUser', JSON.stringify(usuario));
    localStorage.setItem('currentUser', usuario);
  }

  public verificarSesion():boolean {
    if(localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  public getUsuarios() {
    console.log(this.uriUsuario);
    let uri = 'http://localhost:3000/api/v1/usuario';
    let token = localStorage.getItem('token');
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({'headers':headers});
    headers.append('Authorization', token);
    return this.http.get(uri,options)
    .map(res => {
      console.log(res.json());
      this.usuarios = res.json();
    });
  }
  public agregar(usuario:any) {
    let uri = 'http://localhost:3000/api/v1/usuario';
    let token = localStorage.getItem('token');
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({'headers':headers});
    headers.append('Authorization', token);
    let data = JSON.stringify(usuario);
      this.http.post(uri, data, options)
    .subscribe( res => {
      console.log(res.json());
    }, error => {
      console.log(error.text());
    });

  }
  public editar(usuario:any){
    console.log('Antes de enviar al editar del servidor');
    console.log(JSON.stringify(usuario));
    let uri = 'http://localhost:3000/api/v1/usuario/';
    let uri2 = uri + usuario.idUsuario;
    let token = localStorage.getItem('token');
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({'headers':headers});
    headers.append('Authorization', token);
    let data = JSON.stringify(usuario);
      this.http.put(uri2, data, options)
    .subscribe( res => {
    
    }, error => {
      console.log(error.text());
    });
    
  }
  public eliminar(idUsuario:any){
    console.log("Eliminar Usuario:" + idUsuario);
    let uri = 'http://localhost:3000/api/v1/usuario/';
    let uri2 = uri + idUsuario;
    let token = localStorage.getItem('token');
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({'headers':headers});
    headers.append('Authorization', token);
    this.http.delete(uri2, options)
      .subscribe(res =>{

      },error =>{
        console.log(error.text());
      });
    
  }










}