import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';

import { app_routing } from './app.routes';

import {HttpModule} from "@angular/http";
import {UsuarioService} from './service/usuario.service';
import {CategoriaService} from './service/categoria.service';
import {ContactoService} from './service/contacto.service';
import {TareaService} from './service/tarea.service';
import {CitaService} from './service/cita.service';
import {HistorialService} from './service/historial.service';
import {AutoGuardService} from './service/auth-gard.service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/dashboard/navbar/navbar.component';
import {BodyComponent} from './components/dashboard/body/body.component';
import { FooterComponent } from './components/dashboard/footer/footer.component';
import { HomeComponent } from './components/dashboard/home/home.component';
import { UsuarioComponent } from './components/dashboard/usuario/usuario.component';
import { UsuarioEditarComponent } from './components/dashboard/usuario/usuario-editar.component';
import { UsuarioDetalleComponent } from './components/dashboard/usuario/usuario-detalle.component';
import { ContactoEditarComponent } from './components/dashboard/contacto/contacto-editar.component';
import { ContactoDetalleComponent } from './components/dashboard/contacto/contacto-detalle.component';
import { CategoriaComponent } from './components/dashboard/categoria/categoria.component';
import { ContactoComponent } from './components/dashboard/contacto/contacto.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { SingupComponent } from './components/singup/singup.component';
import { TareaComponent } from './components/dashboard/tarea/tarea.component';
import { TareaDetalleComponent } from './components/dashboard/tarea/tarea-detalle.component';
import { CitaComponent } from './components/dashboard/cita/cita.component';
import { HistorialComponent } from './components/dashboard/historial/historial.component';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BodyComponent,
    FooterComponent,
    HomeComponent,
    UsuarioComponent,
    CategoriaComponent,
    ContactoComponent,
    DashboardComponent,
    LoginComponent,
    SingupComponent,
    UsuarioEditarComponent,
    UsuarioDetalleComponent,
    ContactoEditarComponent,
    ContactoDetalleComponent,
    TareaComponent,
    TareaDetalleComponent,
    CitaComponent,
    HistorialComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    app_routing
    
  ],
  providers: [
    UsuarioService,
    CategoriaService,
    ContactoService,
    AutoGuardService,
    TareaService,
    CitaService,
    HistorialService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
